# Nombre: Julen San Tirso Hernández 
# Entrega: Redes Neuronales
#---------------------------------------------------------------------------
# Limpio todo, elijo el directorio y leo el csv

rm(list=ls())
# Install.packages("neuralnet") para instalarla libreria.
library(neuralnet)
setwd('C:/Users/Usuario/Desktop/Deusto/Primersemestre/Inteligencia artificial/Diapositivas/Entregables/Entregable 5')
hormigon = read.csv("hormigon.csv")

# Hago la funcion para normalizar los datos del hormigon
NormalizarDatos=function(dato){
  cant1 = dim(dato)[2];
  cant2 = dim(dato)[1];
  for (i in 1:cant1) {
    max = apply(dato,2,max)[i]
    min = apply(dato,2,min)[i]
    
    for (j in 1:cant2) {
      dato[j,i] = (dato[j,i] - min)/(max - min)
    }
  }
  dato
}

# Normalizo los datos del hormigon

hormigon_norm = NormalizarDatos(hormigon)

# Calculo el numero de filas del documento csv y asi mas adelante calculamos el 80% de las filas y el 20% para dividir el csv en entrenamiento y test

k=dim(hormigon_norm)

n= k[1] * 0.8
i= k[1] * 0.2
j= 9

# Obtengo las nuevas matrices entrenamiento y test con los porcentajes anteriores

entrenamiento = matrix(0,n,j)
test = matrix(0,i,j)

entrenamiento = hormigon_norm[1:n,]

# Para test uso la funci?n tail, para que obtenga los de la cola

test = tail(hormigon_norm, n = i)

# Divido test, asi podre usarlo por un lado sin la variable Strength(test2) 
# y por otro solo con la variable Strength(test3)

test2 =test[,-9]
test3=test[,9]

# Entreno la neurona para la fuerza del cemento= Stregth a la que le van a influir el resto de las variables
# Para ello tenemos la funci?n neuralnet, con la funcion que usaremos, los datos de entrenamiento y el numero de neuronas y capas

red = neuralnet("strength ~ cement + slag + ash + water + superplastic + coarseagg + fineagg + age", entrenamiento, hidden = c(6,4,3,2))

# Dibujo la neurona

plot(red)

# Obtengo los resultados que me da la neurona ya entrenada con los datos de test sin el Strength

results=compute(red,test2)

# Y ahora miro la correlaci?n entre los resultados anteriores(los de la neurona entrenada) y los valores de Strength que ya venian en el apartado test

cor(test3,results$net.result,use= "everything")

# Ahora realizo diferentes pruebas para calcular los datos con diferentes capas y diferentes neuronas
# Para eso modifico en la funcion neuralnet el valor de Hiden c(Aqui modifico la cantidad de neuronas y capas)
# Para 1 neurona c(1), Para dos capas con 1 neurona c(1,1) para dos capas y una neurona en la primera y 3 en la segunda c(1,3)....
