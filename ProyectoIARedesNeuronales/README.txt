COMPONENTES DEL PROYECTO

	- 3 Scripts

		-Procesamiento de datos: Contiene todo el proceso de tratamiento de datos para convertir una matriz de noticias
		en una matriz de frecuencia con todas las palabras y su frecuencia de forma normalizada.
		
		-ProcesamientoNeuronal: Contiene el proceso de las redes neuronales, todo el trabajo realizado para obtener los
		resultados que nos indicaran si podemos acertar en nuestras predicciones o no.

		-Brokers&Students.Rmd:  Este Script contiene el c�digo para generar el HTML del informe oficial que generamos.

	
	- 6 CSV

		-Combined_News_DJIA.csv: Contiene las noticias con la columna Label(tendencia).

		-RedditNews.csv: Contiene las noticias recopiladas a lo largo de 8 a�os sobre DOW JONES.

		-Matriz normalizada.csv: Contiene la matriz normalizada, obtenida de el procesamiento de los datos.

		-PalabrasFrecuentes.csv: Contiene las 18 palabras m�s frecuentes que usaremos para dibujar los gr�ficos y Wordcloud.

		-Entrenamiento.csv: Contiene el tratamiento de los datos sin los pasos de Aggregate, merge y normalizado
		es un paso anterior a Matriz normalizada.csv.

		-TablaResultados.csv: Contiene los resultados de los procesos neuronales, con el tipo de neurona y su correlaci�n.


	-Brokers_Students.html: Es el informe generado en HTML por el Script: Brokers&Students.Rmd.

 
	-Carpeta de im�genes: Contiene im�genes para realizar el informe y la documentaci�n.

	-Carpeta de documentaci�: Contiene la documentaci�n del proyecto.

	-Carpeta Slide: Contiene la portada del proyecto.


Rub�n Ares y Julen San tirso.